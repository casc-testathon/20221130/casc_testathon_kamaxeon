---
controller_templates:
  - name: "{{ orgs }} JT_CasC_Ctrl_CD_Webhook_Trigger"
    description: "Template to attend Controller CasC webhook"
    organization: "{{ orgs }}"
    project: "{{ orgs }} CasC_Data"
    inventory: "{{ orgs }} Localhost"
    playbook: "casc_ctrl_cd_webhook_trigger.yml"
    job_type: run
    fact_caching_enabled: false
    credentials:
      - "{{ orgs }} {{ env }} AAP Credential"
    concurrent_jobs_enabled: true
    ask_scm_branch_on_launch: true
    extra_vars:
      ansible_python_interpreter: /usr/bin/python3
      ansible_async_dir: /home/runner/.ansible_async/
    execution_environment: "ee-casc"

  - name: "{{ orgs }} JT_CasC_Ctrl_Config"
    description: "Template to deploy Controller objects in Org {{ orgs }}"
    organization: "{{ orgs }}"
    project: "{{ orgs }} CasC_Data"
    inventory: "{{ orgs }} Localhost"
    playbook: "casc_ctrl_config.yml"
    job_type: run
    fact_caching_enabled: false
    credentials:
      - "{{ orgs }} {{ env }} AAP Credential"
      - "{{ orgs }} {{ env }} Vault Credential"
    concurrent_jobs_enabled: true
    ask_scm_branch_on_launch: true
    ask_tags_on_launch: true
    ask_verbosity_on_launch: true
    ask_variables_on_launch: true
    extra_vars:
      ansible_python_interpreter: /usr/bin/python3
      ansible_async_dir: /home/runner/.ansible_async/
    execution_environment: "ee-casc"

  - name: "{{ orgs }} JT_CasC_Ctrl_Drop_Diff"
    description: "Template to assure no difference between API and repository"
    organization: "{{ orgs }}"
    project: "{{ orgs }} CasC_Data"
    inventory: "{{ orgs }} Localhost"
    playbook: "casc_ctrl_drop_diff.yml"
    job_type: run
    fact_caching_enabled: false
    credentials:
      - "{{ orgs }} {{ env }} AAP Credential"
      - "{{ orgs }} {{ env }} Vault Credential"
    concurrent_jobs_enabled: true
    ask_scm_branch_on_launch: true
    ask_tags_on_launch: true
    ask_verbosity_on_launch: true
    ask_variables_on_launch: true
    extra_vars:
      ansible_python_interpreter: /usr/bin/python3
      ansible_async_dir: /home/runner/.ansible_async/
    execution_environment: "ee-casc"

  - name: "{{ orgs }} JT_Gitlab_Webhook_Creation"
    description: "Template to create webhook in the gitlab project"
    organization: "{{ orgs }}"
    project: "{{ orgs }} CasC_Data"
    inventory: "{{ orgs }} Localhost"
    playbook: "gitlab_webhook.yml"
    job_type: run
    fact_caching_enabled: false
    credentials:
      - "{{ orgs }} {{ env }} AAP Credential"
      - "{{ orgs }} {{ env }} GitLab API Token"
    concurrent_jobs_enabled: true
    extra_vars:
      ansible_python_interpreter: /usr/bin/python3
      ansible_async_dir: /home/runner/.ansible_async/
      gitlab_api_user: ""
    execution_environment: "ee-casc"
    survey_enabled: True
    survey_spec:
      name: 'Survey_Gitlab_Webhook_Creation'
      description: 'Survey_Gitlab_Webhook_Creation'
      spec:
        - question_name: gitlab_api_url
          required: true
          type: multiplechoice
          variable: gitlab_api_url
          min: 0
          max: 1024
          choices:
            - "https://gitlab.com"
          default: "https://gitlab.com"
          new_question: true

        - question_name: gitlab_group
          required: true
          type: multiplechoice
          variable: gitlab_group
          min: 0
          max: 1024
          choices:
            - "CasC-Testathon"
          default: "CasC-Testathon"
          new_question: true

        - question_name: gitlab_subgroup
          required: true
          type: multiplechoice
          variable: gitlab_subgroup
          min: 0
          max: 1024
          choices:
            - "20221130"
          default: "20221130"
          new_question: true

        - question_name: Trigger hook on push events?
          required: true
          type: multiplechoice
          variable: gitlab_action_push
          min: 0
          max: 30
          choices: "true\nfalse"
          default: "{{ 'true' if env != 'pro' else 'false' }}"
          new_question: true

        - question_name: gitlab_action_tag
          required: true
          type: multiplechoice
          variable: gitlab_action_tag
          min: 0
          max: 1024
          choices: "true\nfalse"
          default: "{{ 'false' if env != 'pro' else 'true' }}"
          new_question: true

        - question_name: gitlab_branch_filter
          required: true
          type: multiplechoice
          variable: gitlab_branch_filter
          choices: "dev\npro"
          default: "{{ 'dev' if env != 'pro' else 'pro' }}"
          new_question: true

        - question_name: "Organization / Gitlab Project Name"
          required: true
          type: text
          variable: gitlab_project
          default: "{{ orgs }}"
          min: 0
          max: 40
          new_question: false

        - question_name: "workflow_job_template_name"
          required: true
          type: text
          variable: "workflow_job_template_name"
          default: "{{ orgs }} WF_CasC_Ctrl"
          min: 0
          max: 60
          new_question: true
...
